# Jekyll Docker #

Docker Compose setup that creates a Jekyll container from Jekyll's Github pages.

### What is this repository for? ###

* Used to serve Jekyll sites in a Docker container

### How do I get set up? ###

* Clone this Repo
* Create a folder named `site` in the same folder as docker-compose.yml
* Copy your Jekyll site into the folder you just created
* Run `sudo docker-compose up` in the same folder as docker-compose.yml